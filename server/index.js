const path = require('path');
const port = process.env.PORT || 8080;
const server = require('./server');
const app = server({
  outputPath: path.resolve(process.cwd(), 'build'),
  publicPath: '/',
});

app.listen(port);
