module.exports = options => {
  const isProd = process.env.NODE_ENV === 'production';

  if (isProd) {
    const getProdServer = require('./prodServer');
    return getProdServer(options);
  } else {
    const webpackConfig = require('webpack/webpack.dev');
    const getDevServer = require('./devServer');
    return getDevServer(webpackConfig);
  }
};
