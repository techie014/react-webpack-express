const fs = require('fs');
const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

module.exports = config => {
  const app = express();
  const compiler = webpack(config);
  app.use(
    webpackDevMiddleware(compiler, {
      noInfo: true,
      publicPath: config.output.publicPath,
      stats: {
        all: false,
        colors: true,
        errors: true,
        errorDetails: true,
        performance: true,
        timings: true,
        warnings: true,
      },
    })
  );
  app.use(webpackHotMiddleware(compiler));

  return app;
};
