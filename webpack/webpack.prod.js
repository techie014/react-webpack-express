const path = require('path');
const webpack = require('webpack');
const webpackBase = require('./webpack.base');

const webpackProdOptions = {
  mode: 'production',
  devtool: 'source-map',
};

module.exports = webpackBase(webpackProdOptions);
