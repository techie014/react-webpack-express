const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const webpackBase = options => {
  const mode = options.mode || 'production';
  const entry = options.entry || ['./src/index.js'];
  const output = {
    path: path.resolve(process.cwd(), 'build'),
    filename: 'bundle.js',
    publicPath: '/',
    ...options.output,
  };
  const module = {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react'],
          },
        },
      },
      {
        test: /\.html$/,
        use: 'html-loader',
      },
    ],
  };
  const plugins = [
    new HtmlWebpackPlugin({ inject: true, template: 'src/index.html' }),
  ].concat(options.plugins ? options.plugins : []);
  const devtool = options.devtool || 'source-map';
  const stats = {
    errors: true,
    chunks: false,
    colors: true,
    entrypoints: true,
    errorDetails: true,
    modules: false,
    performance: true,
    timings: true,
    warnings: true,
  };

  return {
    mode,
    entry,
    output,
    module,
    plugins,
    devtool,
    stats,
    target: 'web',
  };
};

module.exports = webpackBase;
