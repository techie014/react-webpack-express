var path = require('path');
var webpack = require('webpack');
var webpackBase = require('./webpack.base');

const webpackDevOptions = {
  mode: 'development',
  output: {
    path: '/',
    filename: 'bundle.js',
    publicPath: '/',
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
};

module.exports = webpackBase(webpackDevOptions);
